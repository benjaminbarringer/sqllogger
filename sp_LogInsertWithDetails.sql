CREATE procedure [dbo].[sp_LogInsertWithDetails] 
@sessionHash uniqueidentifier,
@startDate datetime,
@endDate datetime,
@func varchar(30),
@recordCount int,
@type varchar(3),
@details XML = null,
@logUUID uniqueidentifier OUTPUT
as
begin

	declare @execTime int = datediff(MILLISECOND, @startDate, @endDate)
	
	set @logUUID = newid()

	set nocount on
	insert into tblLog(logUUID, sessionHash, func, execTime, recordCount, type)
	values(@logUUID, @sessionHash, @func, @execTime, @recordCount, @type)


	if (@details is not null)
	begin
		insert into tblLogDetails(logUUID, msg, type, dateCreated)
		SELECT @logUUID, t.c.value('(@msg)', 'varchar(25)') AS msg, 
				t.c.value('(@logType)', 'varchar(3)') as logType, 
				t.c.value('(@datecreated)', 'datetime') 
		FROM   @details.nodes('/data/*') T(c)
	end

	set nocount off	
end

