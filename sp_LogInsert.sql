CREATE procedure [dbo].[sp_LogInsert] 
@sessionHash uniqueidentifier,
@startDate datetime,
@endDate datetime,
@func varchar(30),
@recordCount int,
@type varchar(3) = 'ACT',
@logUUID uniqueidentifier OUTPUT,
@serverName [sysname] =  null,
@hostName [sysname] = null,
@userName [sysname] = null,
@spid [sysname] = null

as
begin

	declare @execTime int = datediff(MILLISECOND, @startDate, @endDate)
	
	set @logUUID = newid()

	set nocount on
	insert into tblLog(logUUID, sessionHash, func, execTime, recordCount, type, serverName, hostName, userName, spid)
	values(@logUUID, @sessionHash, @func, @execTime, @recordCount, @type, @serverName, @hostName, @userName, @spid)

	set nocount off
	
end
