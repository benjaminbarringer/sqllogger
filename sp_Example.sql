create procedure [dbo].[sp_Example]
@sessionHash uniqueidentifier
as
begin

	set nocount on
	
	declare @logStartDate datetime = getdate(),
			@logRc int = 0,
			@logType varchar(3) = 'ACT'


	--some error do this
	if not exists(select 1)
	begin
		set nocount off
		select 'could not find ?' as error
		set @logType = 'ERR'
		goto logStuff
	end
	
	
	set nocount off
	--Do update, select or inserts here.
	
	set @logRc = @@ROWCOUNT

	
logStuff:
	set nocount on 
	declare @logEndDate datetime = getdate(), @obj varchar(30) = object_name(@@PROCID), @logUUID uniqueidentifier, @sessionHash uniqueidentifier = newid(), @host_name sysname = host_name(), @suser_sname sysname = suser_sname()
	exec dbmon.dbo.sp_loginsert @sessionHash, @logStartDate, @logEndDate,@obj, @logRc, @logType, @logUUID OUTPUT, @@servername, @host_name, @suser_sname, @@spid 		
	
end
