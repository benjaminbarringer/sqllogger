
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

USE [DBMON]
GO

/****** Object:  Table [dbo].[tblLog]    Script Date: 5/10/2016 11:19:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblLog](
	[logId] [int] IDENTITY(1,1) NOT NULL,
	[logUUID] [uniqueidentifier] NULL,
	[sessionHash] [uniqueidentifier] NULL,
	[func] [varchar](30) NULL,
	[execTime] [int] NULL,
	[dateCreated] [datetime] NULL,
	[recordCount] [int] NULL,
	[type] [varchar](3) NULL,
	[hasError] [bit] NULL,
	[serverName] [sysname] NULL,
	[hostName] [sysname] NULL,
	[userName] [sysname] NULL,
	[spid] [sysname] NULL,
PRIMARY KEY CLUSTERED 
(
	[logId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblLog] ADD  DEFAULT (newid()) FOR [logUUID]
GO

ALTER TABLE [dbo].[tblLog] ADD  DEFAULT ((0)) FOR [execTime]
GO

ALTER TABLE [dbo].[tblLog] ADD  DEFAULT (getdate()) FOR [dateCreated]
GO

ALTER TABLE [dbo].[tblLog] ADD  DEFAULT ((0)) FOR [recordCount]
GO

ALTER TABLE [dbo].[tblLog] ADD  DEFAULT ((0)) FOR [hasError]
GO



GO



CREATE TABLE [dbo].[tblLogDetails](
	[logDetailId] [int] IDENTITY(1,1) NOT NULL,
	[logDetailUUID] [uniqueidentifier] NULL CONSTRAINT [DF_tblLogDetails_logDetailUUID]  DEFAULT (newid()),
	[type] [char](3) NULL,
	[dateCreated] [datetime] NULL DEFAULT (getdate()),
	[msg] [varchar](255) NULL,
	[logUUID] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[logDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO




